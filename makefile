SIM =_sim


all:
	sgxsdk/bin/x64/sgx_edger8r --untrusted Enclave.edl --search-path ./ --search-path sgxsdk/include

	cc -m64 -O2  -fPIC -Wno-attributes -IInclude -IApp -Isgxsdk/include -DNDEBUG -UEDEBUG -UDEBUG -c Enclave_u.c -o Enclave_u.o


	g++ -m64 -O2  -fPIC -Wno-attributes -IInclude -IApp -Isgxsdk/include -DNDEBUG -UEDEBUG -UDEBUG -std=c++11 -c App.cpp -o App.o


	g++ Enclave_u.o App.o  -o app -m64 -O0  -Lsgxsdk/lib64 -lsgx_urts$(SIM) -lpthread  -lsgx_uae_service$(SIM)

	sgxsdk/bin/x64/sgx_edger8r --trusted Enclave.edl --search-path ./ --search-path sgxsdk/include

	cc -m64 -O0  -nostdinc -fvisibility=hidden -fpie -fstack-protector   -IInclude -IEnclave -Isgxsdk/include -Isgxsdk/include/tlibc -Isgxsdk/include/stlport  -Isgxsdk/include/IPP -c Enclave_t.c -o Enclave_t.o


	g++ -m64 -O0  -nostdinc -fvisibility=hidden -fpie -fstack-protector-strong   -IInclude -IEnclave -Isgxsdk/include -Isgxsdk/include/tlibc -Isgxsdk/include/stlport   -Isgxsdk/include/IPP    -std=c++03 -nostdinc++ -c Enclave.cpp -o Enclave.o

	g++ Enclave_t.o Enclave.o -o enclave.so -m64 -O2 -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles -Lsgxsdk/lib64 -Wl,--whole-archive -lsgx_trts$(SIM)   -Wl,--no-whole-archive -Wl,--start-group -lsgx_tstdc -lsgx_tstdcxx -lsgx_tcrypto -lsgx_tservice$(SIM) -Wl,--end-group -Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined -Wl,-pie,-eenclave_entry -Wl,--export-dynamic -Wl,--defsym,__ImageBase=0 -Wl,--gc-sections  -Wl,--version-script=Enclave.lds

	sgxsdk/bin/x64/sgx_sign sign -key Enclave_private.pem -enclave enclave.so -out enclave.signed.so -config Enclave.config.xml



clean:
	rm -f *.o app *# Enclave_t.* Enclave_u.* enclave.so enclave.signed.so *~
